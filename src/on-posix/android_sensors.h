//
// Created by Soufian on 02/08/2020.
//
#pragma once

#ifdef  __ANDROID__

#include "rodos.h"
#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dlfcn.h>
#include <assert.h>
#include <android/sensor.h>
#include <android_native_app_glue.h>
#include <android/log.h>

#define DEFAULT_SAMPLING_FREQENCY 60

typedef ASensorEvent SensorData;

/**
 * (Android related)get the intial Sensormanager with help of JNI
 * @param app is a struct we get from the android_main
 * @return  the ASensorManager
 */


enum SENSORTYPE {
    INVALID_TYPE = -1,
    ACCELEROMETER = 1,
    MAGNETIC_FIELD = 2,
    GYROSCOPE = 4
};

/**
 * Simple android sensor abstraction implementing a thread to deliver sensor data
 */
class Sensor : public StaticThread<> {
public:
    SENSORTYPE type;
    Topic<SensorData> sensorDataTopic = Topic<SensorData>(-1, "sensor_data_topic");
    int samplingFrequency;

    /**
* A thread that acts like a Sensor with its own Topic that it published
* on if new Sensorevents arrive.
* @param type choose Sensor type from the enum above
* @param samplingFrequency_Hz with default value of 60 Hz
* @param name of the thread
* @param priority for thread
*/

    Sensor(SENSORTYPE type, int samplingFrequency_Hz = DEFAULT_SAMPLING_FREQENCY,
           const char *name = "AnonymThread",
           const int32_t priority = DEFAULT_THREAD_PRIORITY);

/** to enable or disable the sensor use the  following 2 methods respectively
 * @return 0 on success or a negative error code on failure.
 * */
    int enable();

    /**
* make sure to disable to disable Sensor when not in use (multiple Seconds)
* @return 0 on success or a negative error code on failure.
*/
    int disable();

    /**
* to set how many samplin the sensor makes per Second, default is 60 Hz
* be aware that the real frequency will generally be a bit higher.
* @param frequncy
* @return 0 on sucess or a negative error code on failure.
*/
    int setSamplingFrequency(int frequncy);

    /**
*  Prints basic information about this sensor
*/
    void printInfo();

    void init();

    void run();

private:
    ASensorManager *manager;
    const ASensor *sensor;
    ASensorEventQueue *sensorEventQueue;
    bool isEnabled = false;


};


#endif // __Android
