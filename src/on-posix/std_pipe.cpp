//
// Created by Soufian on 08/07/2020.
//
#ifdef __ANDROID__
#include <jni.h>
#include <android_native_app_glue.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <android/log.h>
#include <pthread.h>

//use pfd[0] for read and pfd[1] for write
static int pfd[2];
static pthread_t thr;
static const char *tag = "rodosAndroid";

static void *thread_func(void*)
{
    ssize_t read_size;
    char buf[256];
    while((read_size = read(pfd[0], buf, sizeof buf - 1)) > 0) {
        if(buf[read_size - 1] == '\n') --read_size;
        buf[read_size] = 0;  /* add null-terminator */
        __android_log_write(ANDROID_LOG_INFO, tag, buf);
    }
    return 0;
}

 int start_logger()
{


    // make stdout line-buffered and stderr unbuffered
    setvbuf(stdout, 0, _IOLBF, 0);
    setvbuf(stderr, 0, _IONBF, 0);

    // create the pipe and redirect stdout and stderr
    if(pipe(pfd)==-1)
        __android_log_write(ANDROID_LOG_DEBUG, tag, "Error while crating the logger pipe");

    dup2(pfd[1], 1);
    dup2(pfd[1], 2);

    // spawn the logging thread
    if(pthread_create(&thr, 0, thread_func, 0) == -1){
        __android_log_write(ANDROID_LOG_DEBUG, tag, "An error occured trying to spawn logger thread");
        return -1;} //if it fails to crate the pthread don't detach
    pthread_detach(thr);
    return 0;
}

#endif



