//
// Created by Soufian on 09/08/2020.
//
#include <android_native_app_glue.h>

#include <jni.h>
#include <android/log.h>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include "rodos.h"
#include "android/native_activity.h"

#ifdef __ANDROID__
extern int main (int argc, char** argv) ;
pthread_t rodosMainTh;



extern int start_logger();

android_app *state;
void signalHandler(int signal){
    pthread_kill(rodosMainTh,signal);
    return;
}


static void handle_Command(struct android_app* app, int32_t cmd){

}
int32_t app_handle_event(struct android_app* app, AInputEvent* event) {
    /*if (AKeyEvent_getKeyCode(event) == AKEYCODE_BACK) {
        ANativeActivity_finish(app->activity);
        return 1;
    };*/

    return 0;
}
void android_main(struct android_app *pApp) {
    state=pApp;
    rodosMainTh =pthread_self();
    signal(SIGALRM,&signalHandler);
    signal(SIGUSR1,&signalHandler);
    signal(SIGIO,&signalHandler);
    //signal(SIGSEGV,&signalHandler);
    start_logger();
    //if you are interested to control the app/activity behaviour in android
    pApp->onAppCmd=handle_Command;
   // pApp->onInputEvent=app_handle_event;

    main(0,NULL);
  //  (void) pApp;
}





/*****************start of  app related code*****************************/



extern "C" JNIEXPORT jstring JNICALL
Java_com_example_rodosandroid_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    char* hello = "Start RODOS!";
    return env->NewStringUTF(hello);
}



#endif
