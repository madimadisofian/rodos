//
// Created by Soufian on 26/07/2020.
//
#ifdef __ANDROID__

#include <android_sensors.h>


#define DEFAULT_SAMPLING_FREQENCY 60
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "DEBUGINFOsofian: ", __VA_ARGS__))




ASensorManager *AcquireASensorManagerInstance(android_app *app) {

    if (!app)
        return nullptr;

    typedef ASensorManager *(*PF_GETINSTANCEFORPACKAGE)(const char *name);
    void *androidHandle = dlopen("libandroid.so", RTLD_NOW);
    auto getInstanceForPackageFunc = (PF_GETINSTANCEFORPACKAGE)
            dlsym(androidHandle, "ASensorManager_getInstanceForPackage");
    if (getInstanceForPackageFunc) {
        JNIEnv *env = nullptr;
        app->activity->vm->AttachCurrentThread(&env, nullptr);

        jclass android_content_Context = env->GetObjectClass(app->activity->clazz);
        jmethodID midGetPackageName = env->GetMethodID(android_content_Context,
                                                       "getPackageName",
                                                       "()Ljava/lang/String;");
        auto packageName = (jstring) env->CallObjectMethod(app->activity->clazz,
                                                           midGetPackageName);

        const char *nativePackageName = env->GetStringUTFChars(packageName, nullptr);

        ASensorManager *mgr = getInstanceForPackageFunc(nativePackageName);
        env->ReleaseStringUTFChars(packageName, nativePackageName);
        app->activity->vm->DetachCurrentThread();
        if (mgr) {
            dlclose(androidHandle);
            return mgr;
        }
    }
}

extern android_app *state;

Sensor::Sensor(SENSORTYPE type, int samplingFrequency_Hz, const char *name, int32_t priority): StaticThread(name, priority) {
    this->type = type;
    this->samplingFrequency = samplingFrequency_Hz;
}

    int Sensor::enable() {
    int ret = ASensorEventQueue_enableSensor(sensorEventQueue, sensor);
    setSamplingFrequency(samplingFrequency);
    isEnabled = true;
    return ret;
}

int Sensor::disable() {
    return ASensorEventQueue_disableSensor(sensorEventQueue, sensor);
}

int Sensor::setSamplingFrequency(int frequncy) {
    this->samplingFrequency = frequncy;

    return ASensorEventQueue_setEventRate(sensorEventQueue, sensor,
                                          (int32_t) (1000000 / samplingFrequency));


}

void Sensor::init() {
    manager = AcquireASensorManagerInstance(state);

    manager = AcquireASensorManagerInstance(state);
    sensor = ASensorManager_getDefaultSensor(manager, type);
    sensorEventQueue = ASensorManager_createEventQueue(manager, state->looper, LOOPER_ID_USER,
                                                       nullptr, nullptr);

}

void Sensor::run() {
    auto sensorLooper = ALooper_prepare(0);
    ALooper_wake(sensorLooper);
    struct android_poll_source *source;
    int events;
    ASensorEvent event;
    int getEventreturn;
    long long cycles = 0;
    auto t_0 = NOW();
    while (true) {
        if (int ss = ALooper_pollAll(0, nullptr, &events, (void **) &source) == -4)
            PRINTF("something went wrong with the sensor\n");
        if (sensor != nullptr) {
            getEventreturn =
                    ASensorEventQueue_getEvents(sensorEventQueue, &event, 1) > 0;
            while (getEventreturn) {
                // PRINTF("accelerometer: x=%f y=%f z=%f\n", event.acceleration.x,
                //      event.acceleration.y, event.acceleration.z);
                sensorDataTopic.publish(event);

                getEventreturn =
                        ASensorEventQueue_getEvents(sensorEventQueue, &event, 1) > 0;
            }
        }
        cycles++;
        suspendCallerUntil(t_0 + cycles * MILLISECONDS);


    }
}
void Sensor::printInfo() {
    PRINTF(".\n   ------SENSOR INFO------\n");
    PRINTF("name is: %s.\n",ASensor_getName(sensor));
    PRINTF("Type is: %s.\n",ASensor_getStringType(sensor));
    PRINTF("Resolution is: %.8f.\n",ASensor_getResolution(sensor));
    PRINTF("Vendor is: %s.\n",ASensor_getVendor(sensor));
    PRINTF("Fifo Max event count: %d.\n",ASensor_getFifoMaxEventCount(sensor));
    PRINTF("   -----------------------\n");

}


# endif